/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@push.rocks/smartobject',
  version: '1.0.12',
  description: 'work with objects'
}
