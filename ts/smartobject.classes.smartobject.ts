import * as tools from './tools/index.js';

/**
 * a smartobject that simplifies accessing objects
 */
export class SmartObject {
  // instance
  public originalObject: object;
  constructor(originalObjectArg: object) {
    this.originalObject = originalObjectArg;
  }

  public getValueAtFlatPathString(pathArg: string) {
    return tools.smartGet(this.originalObject, pathArg);
  }

  public addValueAtFlatPathString(pathArg: string, valueArg: any) {
    return tools.smartAdd(this.originalObject, pathArg, valueArg);
  }

  public toFlatObject() {
    return tools.toFlatObject(this.originalObject);
  }
}
