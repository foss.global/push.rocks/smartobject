// thirdparty scope
// tslint:disable-next-line: no-submodule-imports
import fastDeepEqual from 'fast-deep-equal/es6/index.js';
import * as minimatch from 'minimatch';

export { fastDeepEqual, minimatch };
