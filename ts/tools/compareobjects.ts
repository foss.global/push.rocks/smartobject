import * as plugins from '../smartobject.plugins.js';

export interface IObjectCompareResult {
  presentInBothProperties: string[];
  missingProperties: string[];
  additionalProperties: string[];
  nulledProperties: string[];
  undefinedProperties: string[];
  divergingProperties: string[];
  equalProperties: string[];
}

export const compareObjects = (
  referenceObjectArg: any,
  comparisonObjectArg: any
): IObjectCompareResult => {
  const returnComparisonObject = {
    missingProperties: [] as string[],
    additionalProperties: [] as string[],
    presentInBothProperties: [] as string[],
    nulledProperties: [] as string[],
    undefinedProperties: [] as string[],
    divergingProperties: [] as string[],
    equalProperties: [] as string[],
  };

  const allProperties = Object.keys(referenceObjectArg).concat(Object.keys(comparisonObjectArg));
  for (const currentProperty of allProperties) {
    // lets find presentInBothProperties
    if (referenceObjectArg[currentProperty] && comparisonObjectArg[currentProperty]) {
      returnComparisonObject.presentInBothProperties.push(currentProperty);
    }

    // lets find missingProperties
    if (referenceObjectArg[currentProperty] && !comparisonObjectArg[currentProperty]) {
      returnComparisonObject.missingProperties.push(currentProperty);
    }

    // lets find additionalProperties
    if (!referenceObjectArg[currentProperty] && comparisonObjectArg[currentProperty]) {
      returnComparisonObject.additionalProperties.push(currentProperty);
    }

    // lets find nulledProperties
    if (comparisonObjectArg[currentProperty] === null) {
      returnComparisonObject.nulledProperties.push(currentProperty);
    }

    // lets find undefinedProperties
    if (comparisonObjectArg[currentProperty] === undefined) {
      returnComparisonObject.undefinedProperties.push(currentProperty);
    }

    // lets find divergingProperties
    if (
      JSON.stringify(referenceObjectArg[currentProperty]) !==
      JSON.stringify(comparisonObjectArg[currentProperty])
    ) {
      returnComparisonObject.divergingProperties.push(currentProperty);
    }

    // lets find equalProperties
    if (
      plugins.fastDeepEqual(
        referenceObjectArg[currentProperty],
        comparisonObjectArg[currentProperty]
      )
    ) {
      returnComparisonObject.equalProperties.push(currentProperty);
    }
  }

  for (const currentProperty of Object.keys(returnComparisonObject)) {
    const onlyUnique = (value: any, index: number, self: Array<any>) => {
      return self.indexOf(value) === index;
    };
    const uniqueArray =
      returnComparisonObject[currentProperty as keyof typeof returnComparisonObject].filter(
        onlyUnique
      );
    returnComparisonObject[currentProperty as keyof typeof returnComparisonObject] = uniqueArray;
  }

  return returnComparisonObject;
};
