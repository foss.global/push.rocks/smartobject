import * as plugins from '../smartobject.plugins.js';

/**
 * checks if an object has a parameter with a given key name, returns true if yes.
 * @param parentObject
 * @param childParam
 * @returns {boolean}
 */
export let exists = (parentObject: object, childParam: string): boolean => {
  if (parentObject.hasOwnProperty(childParam)) {
    return true;
  }
  return false;
};
