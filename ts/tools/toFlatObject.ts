export const toFlatObject = (objectArg: object) => {
  const returnObject: { [key: string]: any } = {};
  const extractLayer = (
    subObject: { [key: string]: any },
    pathArg: string,
    loopProtection: object[]
  ) => {
    if (loopProtection.indexOf(subObject) > -1) {
      return;
    }
    if (subObject)
      for (const key of Object.keys(subObject)) {
        let localPathArg = pathArg;
        if (typeof subObject[key] === 'object' && !(subObject[key] instanceof Array)) {
          const newLoopbackArray = loopProtection.slice();
          newLoopbackArray.push(subObject);
          extractLayer(
            subObject[key],
            localPathArg ? (localPathArg += `.${key}`) : key,
            newLoopbackArray
          );
        } else {
          returnObject[localPathArg ? (localPathArg += `.${key}`) : key] = subObject[key];
        }
      }
  };
  extractLayer(objectArg, '', []);
  return returnObject;
};
