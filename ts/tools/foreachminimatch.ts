import * as plugins from '../smartobject.plugins.js';

/**
 * runs a function for all properties of an object whose key matches a regex expression
 * @param parentObjectArg the parent object
 * @param wildcardArg the rege expression to match the property keys against
 * @param callbackArg the function to run with those properties
 */
export let forEachMinimatch = async (
  parentObjectArg: any,
  wildcardArg: string,
  callbackArg: (matchedArg: string) => void
) => {
  let propertyNames = Object.getOwnPropertyNames(parentObjectArg);
  let propertyNamesMatched = propertyNames.filter((propertyNameArg) => {
    return plugins.minimatch.minimatch(propertyNameArg, wildcardArg);
  });
  for (let propertyNameArg of propertyNamesMatched) {
    await callbackArg(parentObjectArg[propertyNameArg]);
  }
};
