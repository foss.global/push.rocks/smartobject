import * as plugins from './smartobject.plugins.js';
const fastDeepEqual = plugins.fastDeepEqual;
export { fastDeepEqual };

export * from './smartobject.classes.smartobject.js';
export * from './tools/index.js';
