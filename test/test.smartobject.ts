import { expect, expectAsync, tap } from '@push.rocks/tapbundle';
import * as smartobject from '../ts/index.js';

let testSmartobject: smartobject.SmartObject;

tap.test('should create a smartobject', async () => {
  const originalObject = {
    test: 'hey',
    deep: {
      yeah: 'so deep',
      evendeeper: {
        sodeep: 2,
        deepArray: ['one array', 'two array'],
      },
    },
  };
  testSmartobject = new smartobject.SmartObject(originalObject);
  testSmartobject.addValueAtFlatPathString('deep.nice', 'yeah that has been added');
  console.log(testSmartobject.originalObject);
  console.log(testSmartobject.toFlatObject());
  expect(testSmartobject.getValueAtFlatPathString('deep.yeah')).toEqual('so deep');
});

tap.start();
