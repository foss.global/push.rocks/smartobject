import { expect, tap } from '@push.rocks/tapbundle';
import * as smartobject from '../ts/index.js';

tap.test('first test', async () => {
  const result = smartobject.compareObjects(
    { thisIsEq: 'wow', thisIsDeepEq: { deeper: 'sodeep' } },
    { thisIsEq: 'wow', thisIsDeepEq: { deeper: 'sodeep' }, hey: 'there' }
  );
  console.log(result);
});

tap.test('should fast deep equal objects', async () => {
  expect(smartobject.fastDeepEqual({ hello: 'yes' }, { hello: 'yes' })).toBeTrue();
  expect(smartobject.fastDeepEqual({ hello: 'yes' }, { hello: 3 })).toBeFalse();
});

let parentObject: any = {};
let childObject: any = {};

tap.test('childObject should not yet be in parentObject', async () => {
  expect(smartobject.exists(parentObject, 'childObject')).toBeFalse();
  parentObject.childObject = childObject;
});

tap.test('childObject should now be in parentObject', async () => {
  expect(smartobject.exists(parentObject, 'childObject')).toBeTrue();
});

tap.test('should be able to deepAdd an childParam', async () => {
  const parentObject = {
    hello: 'there',
  };
  const parentObject2 = smartobject.smartAdd(parentObject, 'wow.za', 'yes');
  console.log(parentObject2);
  expect(smartobject.exists(parentObject2.wow, 'za')).toBeTrue();
});

tap.test('should be able to deep get an item', async () => {
  const testObject = {
    hey: {
      there: {
        so: 'cool',
      },
    },
  };

  const item = smartobject.smartGet(testObject, 'hey.there.so');
  expect(item).toEqual('cool');
});

tap.test('should call properties for minimatched properties', async () => {
  let testObject = {
    matchedOne: 'Hey!',
    matchedTwo: 'this works!',
    notmatched: 'NOT!',
  };

  const matchedStrings: string[] = [];
  await smartobject.forEachMinimatch(testObject, 'matched*', (matchedProperty) => {
    matchedStrings.push(matchedProperty);
    console.log(matchedProperty);
  });

  expect(matchedStrings).toContain('Hey!');
  expect(matchedStrings).toContain('this works!');
  expect(matchedStrings).not.toContain('NOT!');
});

tap.start();
